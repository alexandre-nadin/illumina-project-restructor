"""
    This module manages the current line in the standard output buffer.
    It helps for the logging visibility, reducing flood of information.
"""

import coloured_output as colout
from kmessage import Message
import sys
from tools import string_utils

__author__ = 'alexandre-nadin'


current_line = ""
def updateCurrentLine(content):
    global current_line
    current_line = content

def getCurrentLine():
    global current_line
    return current_line



def appendLine(*args):
    """
    Adds a message to the current line. It does not print any new line.
    :param args:
    :return:
    """
    line = string_utils.stringify_list(args)
    updateCurrentLine(str(getCurrentLine()) + str(line))
    print line,
    sys.stdout.flush()


def printLine(msg):
    """
    Prints a message with new line. Resets the current line.
    :param msg:
    :return:
    """
    updateCurrentLine("")
    print msg
    sys.stdout.flush()


def validateLine(msg=Message.VALIDATED):
    """
    Validates the current line, putting a validating message at its end.
    A new line is then printed.
    :param msg:
    :return:
    """
    printLine("  " + colout.valid(msg))


def invalidateLine(msg=Message.INVALIDATED):
    """
    Invalidates the current line, putting an invalidating message at its end.
    A new line is then printed.
    :param msg:
    :return:
    """
    printLine("  " + colout.error(msg))


def endLine():
    """
    Simply puts a new line.
    :return:
    """
    printLine("")


def eraseLine():
    """
    Erases the content of the current line and put the carret to its beginning.
    :return:
    """
    print '\r' + len(getCurrentLine()) * " ",
    print '\r' + "",
    sys.stdout.flush()


def replaceLine(msg, new_line=False):
    """
    Replaces the current line with a new message.
    Puts a new line if specified.
    :param msg:
    :param new_line:
    :return:
    """
    eraseLine()
    appendLine(msg)
    if new_line:
        endLine()



def test():
    import time
    appendLine("Beginning testing...")
    validateLine()
    appendLine("Test invalidation...")
    time.sleep(1)
    invalidateLine()

    appendLine("first line.")
    appendLine(" continue on same line")
    time.sleep(0.5)
    validateLine()
    time.sleep(1)
    endLine()
    appendLine("2nd line.")
    appendLine(" WIll be deleted soon...")
    time.sleep(1)
    eraseLine()
    time.sleep(1)
    endLine()
    appendLine("3rd line. ")
    appendLine(colout.warn(" WIll be replaced soon!", with_header=True, ansi_codes=colout.AnsiCode.UNDERSCORE))
    time.sleep(3)
    replaceLine(colout.valid("3rd line replaced!"))
    endLine()

if __name__ == '__main__':
    test()

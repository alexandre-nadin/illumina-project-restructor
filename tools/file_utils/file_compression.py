__author__ = 'alexandre-nadin'


import sys, os, file_manipulation as fm
import gzip
from tools import coercer
from tools.pipeline.pipeline import PipelineState

CHUNK_SIZE = 65536
DEFAULT_DESTINATION = "split"

class Extensions:
    GUNZIP = ".gz"
    FASTQ_GZ = ".fastq" + GUNZIP
    BZIP = ".bz"
    ZIP = ".zip"


def split_gz_file(file_names, common_part_name, extension=Extensions.FASTQ_GZ, destination=None, chunks_per_split=10000, lines_per_chunk=4, file_field_separator="_"
                  , zero_padding=3, verbose=False, file_permission=None, execute=True):

    """
    Splits a gz files into different parts.
    :param file_names: the files to split as a whole.
    :param common_part_name: The name that will serve as a base to the generated fragment files.
    :param destination: the folder where to write outputs.
    :param chunks_per_split: max number of chunk to put in each output file.
    :param lines_per_chunk: max number of lines to put in each chunk.
    :param file_field_separator: The pattern separating each field in a file name, usually '_'.
    :param zero_padding: the number of 0 to fill to in the serial number of the file.
    :param verbose:
    :param file_permission:
    :param execute:
    :return:
    """

    absPath = os.path.abspath(file_names[0])
    baseName = os.path.basename(absPath)
    dirName = os.path.dirname(absPath)

    destination_folder = os.path.join(dirName, DEFAULT_DESTINATION) if destination is None \
        else os.path.abspath(destination)

    if not os.path.isdir(destination_folder):
        os.mkdir(destination_folder)
        os.chmod(destination_folder, file_permission)
        if verbose:
            print "Created directory", destination_folder

    # """ Compute fields from the file name """
    # raw_base_name, extensions = baseName.split(os.extsep, 1)
    #
    # if not str(extensions).startswith("."):
    #     extensions = "." + extensions
    #
    # file_fields = str(raw_base_name).split(file_field_separator)
    # first_fields = file_fields[:-1] if file_fields.__len__() > 1 else file_fields
    # first_file_part = file_field_separator.join(first_fields)
    # last_file_field = file_fields[-1] if file_fields.__len__() > 1 else ""
    # current_chunk = coercer.coerce2int(last_file_field)
    # if current_chunk is None or current_chunk < 0:
    #     first_file_part = raw_base_name

    """ Initialize splitting variables """
    lines_per_split = chunks_per_split * lines_per_chunk
    serial_number = 0  # Serial number starts at 0 and is incremented for each smaller file.

    for file_part in get_gzip_chunks(file_names, lines_per_split, verbose):
        serial_number += 1
        fragment_file_path = os.path.join(destination_folder, common_part_name + file_field_separator
                                   + str(serial_number).zfill(zero_padding) + extension)
        if execute:
            if verbose:
                print "  writing chunk: " + str(file_part.__len__()) + " -> ", fragment_file_path

            write_gzip_file(fragment_file_path, file_part, file_permission)
            # if verbose:
            #     print " OK"
        if verbose:
            print "Splitting: created file ", fragment_file_path, "\n"

    return PipelineState.SUCCESS


def get_gzip_chunks(file_names, lines_per_chunk, verbose=False):
    line_counter = 0
    current_chunk = ""
    for file_name in file_names:
        with gzip.open(file_name, 'rb') as f:
            try:
                for line in f:
                    current_chunk += line
                    line_counter += 1
                    if line_counter >= lines_per_chunk:
                        if verbose:
                            print "Got a chunk:", current_chunk.__len__(), "chars at line", line_counter
                        yield current_chunk
                        current_chunk = ""
                        line_counter = 0
            except IOError as ioe:
                print "I/O ERROR rb", file_name, ioe.message
            except ValueError as ve:
                print "VALUE ERROR rb: ", file_name, ve.message
            except EOFError as eofe:
                print "EOF ERROR rb: ", file_name, eofe.message
            except Exception, err:
                print "EXCEPTION rb:", file_name, err.message

    ## Yield remaining lines.
    if not current_chunk == '':
        if verbose:
            print "Got last chunk:", current_chunk.__len__(), "chars at line", line_counter
        yield current_chunk

def get_chunks(content, n):
    """
    Yields successive n-sized chunks from content.
    :param content:
    :param n:
    :return:
    """
    for r in xrange(0, len(content), n):
        yield content[r:r+n]


def write_gzip_file(file_name, content, file_permission=None):
    import gzip
    with gzip.open(file_name, 'wb') as f:
        if not content == '':
            try:
                for chunk in get_chunks(content, CHUNK_SIZE):
                    f.write(chunk)
            except IOError as ioe:
                print "I/O ERROR wb", ioe.message, file_name
            except ValueError as ve:
                print "VALUE ERROR wb", ve.message, file_name
            except EOFError as eofe:
                print "EOF ERROR wb", eofe.message, file_name
            # except Exception, err:
            #     print "EXCEPTION wb:", err.message

    if file_permission is not None and type(file_permission) == int:
        os.chmod(file_name, file_permission)


def get_file_lines(file_name):
    line_counter = 0
    with gzip.open(file_name, 'rb') as f:
        try:
            for line in f:
                line_counter += 1
        except IOError as ioe:
            print "I/O ERROR rb", ioe.message
        except ValueError as ve:
            print "VALUE ERROR rb: ", ve.message
        except EOFError as eofe:
            print "EOF ERROR rb: ", eofe.message
    return line_counter


# --------------------------------------
#               TESTING
# --------------------------------------

def tests(argv=None):
    path = "/Users/alexandre-nadin/dev/illumina-project-restructor/tests/split/"
    dest = path + "output/"
    file = "testToSplit.fastq.gz"
    file = "testToSplit_number.fastq.gz"
    file = "testToSplit_005.fastq.gz"
    file = "testTo_Split.fastq.gz"
    file = "testTo_Split_005.fastq.gz"

    from tools.file_utils import file_manipulation as fm
    file_size = fm.get_file_size(path + file)
    bytes = 0
    for chunk in split_gz_file(path + file, dest, 10000, 4, zero_padding=3, execute=False, verbose=True):
        print "processed ", fm.human_size(len(chunk)), " -> ",
        bytes += len(chunk)
        print " PROGRESS: ", (bytes * 100)/file_size, "%"


def test_split(filename):
    split_gz_file(filename, chunks_per_split=4000000, verbose=True)


def test_get_file_chunk(filename, lines_per_chunk):
    """
    Used to time the difference between reading a gzip file with different lines at a time.

    I found there were no significatn difference.
    Reading a gz file containing 16,424,912, it took
        51.737598896 sec to read it 1 line at a time.
        50.4233710766 sec to read it 4 lines at a time.
        51.1514351368 sec to read it 1,000 lines at a time.

    The tool used to time it was a personal timeit, found in the module performance.timer.py, function name is time_it.

    :param filename:
    :param lines_per_chunk:
    :return:
    """
    counter = 0
    for chunk in get_gzip_chunks(filename, lines_per_chunk):
        counter += 1
    print "counted ", counter, "chunks."

def test_perf():
    file = "/Users/alexandre-nadin/dev/tests/fastq-files/5_CP_S5_L003_R1_001.fastq.gz"
    # test_get_file_chunk(file, 4)
    from performance import timer
    print "[TIME 1 line at a time]"
    timer.time_it(lambda: test_get_file_chunk(file, 1), 1)

    print "[TIME 4 lines at a time]"
    timer.time_it(lambda: test_get_file_chunk(file, 4), 1)

    print "[TIME 1000 lines at a time]"
    timer.time_it(lambda: test_get_file_chunk(file, 1000), 1)


def debugCRC_OEF():
    dirs = [
        "/Volumes/hsr-cluster/lustre2/raw_data/160203_SN859_0258_BC8FJCACXX_old/Project_Coco_62_LungTumorPredisposition/"
        , "/Users/alexandre-nadin/dev/tests/lustre2/raw_data/Runtest/VanAnken_289_RNASeqLite/"
        , "/Volumes/hsr-cluster/lustre2/raw_data/Ciceri_160216_NB501182_0009_AHVTV3BGXX/Project_Ciceri_160_Relapsing_Leukemia/"
        , "/Users/alexandre-nadin/dev/tests/lustre2/raw_data/Runtest/"
    ]
    files = [
        "LT7_S33_L005_R2_001.fastq.gz"
        , "50_L001_R1.fastq.gz"
        , "big_one_001.fastq.gz"
        , "MACL41_S5_L003_R2_001.fastq.gz"
        , "MACL41_S5_L002_R1_001.fastq.gz"
    ]
    lineCounter = []
    # splitGzipFile(dirs[1]+files[2], dest=dirs[1]+"output/", execute=True, verbose=True, chunkPerSplit=500000)

    # splitGzipFile(dirs[2]+files[3], dest=dirs[2]+"Sample_MACL41/", execute=True, verbose=True, chunkPerSplit=4000000, file_permission=0770)
    # splitGzipFile(dirs[2]+files[4], dest=dirs[2]+"Sample_MACL41/", execute=True, verbose=True, chunkPerSplit=4000000, file_permission=0770)
    # splitGzipFile(dirs[2]+files[4], dest="/Users/alexandre-nadin/dev/tests/"+"Sample_MACL41/", execute=True, verbose=True, chunkPerSplit=4000000, file_permission=0770)
    split_gz_file(dirs[3] + files[4], destination=dirs[3], execute=True, verbose=True, chunks_per_split=4000000, file_permission=0770)

    print 'REMOVEING FILE'

def main(argv=None):
    script, args = sys.argv.pop(0), sys.argv
    # tests(argv)
    # debugCRC_OEF()
    # test_perf()
    test_split(args[0])


if __name__ == "__main__":
    print "\n[File Compression] main function"
    sys.exit(main(sys.argv))
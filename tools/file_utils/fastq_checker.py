__author__ = 'alexandre-nadin'


import os, glob, sys
from sets import Set
import HTSeq

import functools
sys.path.append("/Users/alexandre-nadin/dev/illumina-project-restructor")


class FastqCounts:
    def __init__(self, id):
        self._id = id
        self._seq_name_counts = {}  # Contains the counts for each seq.name
        self._seq_content = {}  # Contains the sequence for each seq.seq
        self._dupli_seq_names = Set()  # Contains the seq.names that are duplicated.

    def add_seq(self, seq, record_seq=False):
        """
        Adds a sequence to the list. If it already exists, increments the duplication counters.
        If specified, the full sequence will be stored in a dict.
        :param seq: the sequence with
        :param src_name:
        :return:
        """
        # A sequence has two objects: .seq and .name
        try:
            self._seq_name_counts[seq.name] += 1
        except KeyError:
            self._seq_name_counts[seq.name] = 1
            if record_seq:
                self._seq_content[seq.name] = seq.seq

        if self._seq_name_counts[seq.name] >= 2:
            self._dupli_seq_names.add(seq.name)

    def get_id(self):
        """
        :return: The id. Usually the file name.
        """
        return self._id

    def get_seq_name_counts(self):
        """
        :return: a dic with the count for each sequence name.
        """
        return self._seq_name_counts

    def get_duplicated_seq_names(self):
        return self._dupli_seq_names


def get_files_counters(files):
    common_counter = FastqCounts("Common counter")
    file_counters = []
    for f in files:
        print "Reading file: ", f,
        temp_counter = FastqCounts(id=f)
        freader = HTSeq.FastqReader(f)
        for seq in freader:
            temp_counter.add_seq(seq, record_seq=True)
            common_counter.add_seq(seq, record_seq=False)
        file_counters.append(temp_counter)
        print "OK"
    file_counters.append(common_counter)
    return file_counters


def get_counts(file_path, with_class=False):
    # print "\n[CHECK DUPLICATES] ", file_path
    # print "Reading file..."

    dic_name_counts = {}  # Contains the counts for each seq.name
    dic_name_seq = {}  # Contains the sequence for each seq.seq
    seq_duplicated = Set()  # Contains the seq.names that are duplicated.
    dupli_infiles = Set()  # Contains the files in which the seq.name is duplicated.

    f = HTSeq.FastqReader(file_path)
    fastq_objects = []
    for seq in f:
        # A sequence has two objects: .seq and .name

        try:
            dic_name_counts[seq.name] += 1
        except KeyError:
            dic_name_counts[seq.name] = 1
            dic_name_seq[seq.name] = seq.seq

        dupli_infiles.add(file_path)
        if dic_name_counts[seq.name] >= 2:
            seq_duplicated.add(seq.name)


    # print "Nb seq found: ", dic_name_counts.__len__()
    # print "Nb dupli: ", seq_duplicated.__len__()
    # print "Found in files: ", dupli_infiles
    return (file_path, dic_name_counts, fastq_objects)


def get_counts_time(file_path):
    f = HTSeq.FastqReader(file_path)
    seqs = []
    for seq in f:
        seqs.append(seq)
    return seqs

def get_seq_dict(file_path):
    pass


def readfile(file_name):
    lines = []
    with open(file_name, 'rb') as f:
        lines.append(f.readline())
    return lines

def parse_args(argv):
    ## Check inputs
    # 1: script name
    # 2: file name pattern

    if argv.__len__() < 2:
        print "wrong number of arguments."
        return None
    return argv


def interactive_session(counted_files):
    """
    The idea is to provide the user with the possibility to browse through the stats. In doing so the script does not
    have to be relaunched every time he needs to check one piece of information.
    This should be particularly time efficient when dealing with large files.

    :return:
    """
    # answer = raw_input("your input?\n")
    # print "->", answer
    # print type(answer)
    answer = ''
    while answer != '0':
        output = ''
        output += "Which files you want to intersect?\n"
        for i in range(counted_files.__len__()):
            output += str(i) + " - " + counted_files[i][0] + "\n"
        answer = str(raw_input(output))
        print answer.split()
        # input("Evaluate python code: ")
    print "Exit interactive mode."

def printMan():
    print "\n\tWho: \tfastq checker"
    print "\n\tWhat: \tRead and check fastq files"
    print "\n\tHow: \t$ python script_name file_path_patterns"
    print "\n\tEx: \t$ python fastq_checker.py /path/to/some*/files_*.fastq"
    print "\n"


def count_series(files):
    res = []
    for f in files:
        # res.append(get_counts_time(f))
        res.append(readfile(f))
    return res

def count_multiproc(files):
    from tools.ky_multiprocessing import function_processor as fp
    proc_list = []
    for f in files:
        proc_list.append(
                functools.partial(
                    # get_counts
                    readfile
                    , f

                )
            )
    return fp.map_partial_functions(proc_list)

if __name__ == "__main__":

    parsed = parse_args(sys.argv)
    if parsed is None:
        printMan()
        sys.exit(2)

    script_name, files = parsed.pop(0), parsed


    # folders = ["/Users/alexandre-nadin/dev/tests/generate_fastq/"]
    # file_pool = ["sample-0010.fastq.gz", "sample-doubles.fastq.gz"]
    # files = [folders[0]+file_pool[0], folders[0]+file_pool[1]]

    file_counters = get_files_counters(files)
    for fc in file_counters:
        print "\n[Fastq file]", fc.get_id()
        dupli_seq_names = fc.get_duplicated_seq_names()
        print "Nb. duplicated sequences:", dupli_seq_names.__len__()
        if dupli_seq_names.__len__() > 0:
            for dsn in dupli_seq_names:
                print "\t", dsn, " -> found", fc.get_seq_name_counts()[dsn], "times."

    # if counts.__len__() > 1:
    #     print "\n[CHECK INTERSECTION]"
    #     f1, f2 = counts[0][0], counts[1][0]
    #     s1, s2 = set(counts[0][1].keys()), set(counts[1][1].keys())
    #
    #     inter = set(s1&s2)
    #     print "Seqs intersection:"
    #     for item in inter:
    #         print "\t", item




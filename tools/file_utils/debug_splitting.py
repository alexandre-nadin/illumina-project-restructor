__author__ = 'alexandre-nadin'

import sys, os
import gzip
import file_compression as fc

def checkGzip(file_name):
    with gzip.open(file_name, 'rb') as f:
        lineCounter = 0
        # currentChunk = ""
        for line in f:
            # currentChunk += line
            lineCounter += 1
        #     if lineCounter >= linePerSplit:
        #         yield currentChunk
        #         currentChunk = ""
        #         lineCounter = 0
        # if not currentChunk == '':
        #     yield currentChunk
        print "[OK] went through", lineCounter, "lines in file", file_name

functions=["check-gzip", "split-gzip"]

def main(argv=None):
    # 1: this file
    # 2: function name
    # 3: file name
    # 4: destination
    if argv.__len__() < 3:
        printMan()
        exit(2)
    if argv.__len__() == 3:
        # (launching_cmd, script_name, func_name, file_name) = argv
        (script_name, func_name, file_name) = argv
        destination = "output/"
    else:
        (script_name, func_name, file_name, destination) = argv

    print "args: ", script_name, func_name, file_name, destination, "\n"

    if func_name == functions[0]:
        print "Checking gzip file: ", file_name
        checkGzip(file_name)
    elif func_name == functions[1]:
        fc.split_gz_file(file_name, destination=destination, execute=True, verbose=True, chunks_per_split=4000000, file_permission=0770)
    else:
        print "function name unrecognized: ", func_name
        printMan()
        exit(2)

def printMan():
    print "Launch: $ script_name function-to-call file-name destination"
    print "Available Functions: ", functions

if __name__ == "__main__":
    sys.exit(main(sys.argv))
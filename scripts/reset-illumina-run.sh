#!/bin/bash

## Script informations.
__WHO__=$(basename ${0})
__WHAT__="Restructures illumina's run folder from the old to the new way: Removes all sample folders; Removes SampleSheets
        in projects and run folder; Rename projects' dir name wihtout the project tag."
__HOW__="$ ${__WHO__} run_folder"
__EXAMPLE__="$ ./$__WHO__ run_c-t-g-b"

TAG_PROJECT="Project_"
TAG_SAMPLE="Sample_"
SAMPLE_SHEET="SampleSheet.csv"

get_manual() {
    manual="\n\tWho: ${__WHO__}\n"
    manual+="\n\tWhat: ${__WHAT__}\n"
    manual+="\n\tHow: ${__HOW__}\n"
    manual+="\n\tEx: ${__EXAMPLE__}\n"
    echo "$manual"
}

get_full_path() {
    ## Retrieves a full path. This assumes the path given is accessible from the working directory.
    cur_dir=$(pwd)
    cd ${1}
    full_path=$(pwd)
    cd ${cur_dir}
    echo ${full_path}
}


## Get the expected arguments
P_CMD=${0}
P_RUN=${1}

[[ ! -d ${P_RUN} ]] && echo -e "Error: not valid directory: '${P_RUN}'.\nPlease feed me with a valid run folder." \
&& echo -e $(get_manual) && exit

#####
# THE COMMAND:
# rm -r */Sample_*; rm */SampleSheet.csv; rm SampleSheet.csv; for i in */*.split*; do mv ${i} ${i%*.split*}; done

RUN_DIR=$(get_full_path ${P_RUN})
 "\tremoved projects' sample sheet"

rm -rf ${RUN_DIR}/${SAMPLE_SHEET}
echo -e "\tremoved run's sample sheet"

for project in ${RUN_DIR}/${TAG_PROJECT}*
do
    ## Ignore whatever is not a project folder.
    [[ ! -d ${project} ]] && continue
    echo "PROJECT: ${project}"

    ## Removes sample folders
    rm -rf ${project}/${TAG_SAMPLE}*
    echo -e "\tremoved sample folders"

    ## Removes project's sample sheets
    rm ${project}/${SAMPLE_SHEET}
    echo -e "\tremoved ${project}/${SAMPLE_SHEET}"

    ## Rename to original fastq.gz
    for f in ${project}/*.split*
    do
        mv ${f} ${f%*.split*}
    done
    echo -e "\tmoved all .split* file extensions."


    ## Renames project folder
    p_dirname=$(dirname ${project})
    p_basename=$(basename ${project})
    new_basename=${p_basename#${TAG_PROJECT}*}
    new_project=${p_dirname}/${new_basename}
    mv ${project} ${new_project}
    echo -e "\trenamed ${project}\n\t  -> ${new_project}\n"
done
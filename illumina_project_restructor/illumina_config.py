__author__ = 'alexandre-nadin'

import sys
import os
import inspect
import re
from tools.file_utils import file_manipulation as fm
from tools.ky_output import coloured_output as colout
from tools import string_utils as su


class IlluminaConfig:
    CREATED_FILES_PERMISSION = 0770
    CONFIG_FILE_NAME = "rip.cfg"
    PROJECT_FOLDER_TAG = "Project_"
    SAMPLE_FOLDER_TAG = "Sample_"
    SAMPLE_EXTENSIONS = ["gz", "fqz"]
    PATH_SAMPLE_SHEETS = ["/lustre1/SampleSheets/", 
			  "/Users/nadin.alexandre/my-data/dev/tests/sample-sheets/"
                          ]
    SAMPLE_SHEET_NEW_EXT = "_IEM"  # Tag for new illumina project.
    FCID_POSITION_IN_RUN_NAME = 4
    BLOCK_SPACE_MIN = "50 GB"
    TAKE_OLD_SAMPLE_SHEET_MODEL = False


class CompressedFiles:
    CHUNKS_PER_SPLIT = 16000000
    # CHUNKS_PER_SPLIT = 50000
    # CHUNKS_PER_SPLIT = 10000
    LINES_PER_CHUNK = 4
    PADDING_ZEROS_FOR_SPLITTED_FILES = 3
    DELETE_ORIGINAL_FILES = False


class SampleSheetFieldsNew:
    F_FCID = "FCID"
    F_LANE = "Lane"
    F_SAMPLE_ID = "Sample_ID"
    F_SAMPLE_NAME = "Sample_Name"
    F_SAMPLE_PLATE = "Sample_Plate"
    F_SAMPLE_WELL = "Sample_Well"
    F_INDEX_ID = "I7_Index_ID"
    F_INDEX = "index"
    F_SAMPLE_PROJECT = "Sample_Project"
    F_DESCRIPTION = "Description"

    SAMPLE_FILE_NAME_BASED_ON_SSF = "F_SAMPLE_ID"

    # MANDATORY_FIELDS = [
    #       F_SAMPLE_ID
    #     , F_SAMPLE_PROJECT
    # ]


class SampleSheetFieldsOld:
    F_FCID = "FCID"
    F_LANE = "Lane"
    F_SAMPLE_ID = "SampleID"
    F_SAMPLE_REF = "SampleRef"
    F_INDEX_1 = "Index_1"
    F_DESCRIPTION = "Description"
    F_CONTROL = "Control"
    F_RECIPE = "Recipe"
    F_OPERATOR = "Operator"
    F_SAMPLE_PROJECT = "SampleProject"

    SAMPLE_FILE_NAME_BASED_ON_SSF = "F_SAMPLE_ID"

    # MANDATORY_FIELDS = [
    #       F_SAMPLE_ID
    #     , F_SAMPLE_PROJECT
    # ]


#################
####################
######################
class Configuration:

    TRUE_VALUES = ["true", "t", "yes", "y", 1]
    FALSE_VALUES = ["false", "f", "no", "n", 0]
    MANDATORY_FIELDS = ["SampleSheetFieldsOld.F_SAMPLE_ID", "SampleSheetFieldsOld.F_SAMPLE_PROJECT"
                        , "SampleSheetFieldsNew.F_SAMPLE_ID", "SampleSheetFieldsNew.F_SAMPLE_PROJECT"]

    def __init__(self, module_name=sys.modules[__name__]):
        self._module_name = module_name
        self._class_names = zip(*inspect.getmembers(sys.modules[__name__], inspect.isclass))[0]


    def printClassFields(self):
        for name in self.getFilteredClassNames(self._class_names):
            print "\nName: ", name
            for field in self.getClassVariables(name):
                value = self.getValueOfClassAttribute(name, field)
                msg = field + " = " + ", ".join(value) if isinstance(value, list) else field + " = "\
                                    + str(self.getValueOfClassAttribute(name, field))
                print "  " + msg + "   (" + str(type(value)) + ")"


    def getFilteredClassNames(self, class_names):
        """
        Filters unwanted class names.
        :param class_names:
        :return: new list of filtered class names.
        """
        return [x for x in self._class_names if x != self.__class__.__name__]


    def getClassVariables(self, className):
        """
        Retrieves all the public variables from a class.
        :param className:
        :return:
        """
        try:
            keys = eval(str(className) + ".__dict__.keys()")
            variables = []
            for key in keys:
                if not str(key).startswith("_"):
                    variables.append(key)
            return variables
        except NameError as ne:
            print ne.message


    def getValueOfClassAttribute(self, class_name, attribute):
        """
        Retrieves the value of a class attribute.
        :param class_name:
        :param attribute:
        :return:
        """
        try:
            return eval(str(class_name) + "." + str(attribute))
            # print className + "." + variableName + " = " + eval(str(className) + "." + str(variableName))
        except NameError as ne:
            print "ERROR: " + ne.message


    def evaluate(self, content):
        """
        Evaluates expressions. Used to make sure a class or a class attribute exists.
        :param content:
        :return:
        """
        try:
            return eval(str(content))
        except NameError as ne:
            return None
        except AttributeError as ae:
            return None


    def updateClass(self, class_name, keys_values_dict):
        print "Updating class " + class_name
        if class_name not in self.getFilteredClassNames(self._class_names):
            print colout.error("  ERROR: class " + str(class_name) + " not available.")
            return None

        for key in keys_values_dict:
            if str(key).split(" ").__len__() > 1:
                print colout.error("  ERROR: attribute " + str(key) + " is not suitable.")
            else:
                eval_attr = self.evaluate(str(class_name) + "." + str(key))
                if eval_attr is None:
                    print colout.error("  attribute " + str(key) + " does NOT exist")
                else:
                    self._updateClassAttribute(class_name, key, keys_values_dict[key])
        return 0


    def _updateClassAttribute(self, class_name, attribute, values):
        """
        Updates a class attribute. Expect a list of values as input.
        :param class_name:
        :param attribute:
        :param value:
        :return:
        """
        value = self._getNewClassAttributeValue(class_name, attribute, values)

        current_value = getattr(eval(class_name), attribute)
        print "  ", attribute, ": ", current_value, " -> ", value

        if value is not None:
            try:
                setattr(eval(class_name), attribute, value)
                print "  " + attribute + " -> " + value + " (" + type(value) + ")"
                return 0
            except:
                return None
        else:
            print colout.warn("  cannot update " + str(attribute))


    def _getNewClassAttributeValue(self, class_name, attribute, values):
        """
        Computes the correct values for the class'attribute.
        :param class_name:
        :param attribute:
        :param values:
        :return:
        """
        try:
            eval(class_name)
        except NameError:
            return None

        if not inspect.isclass(eval(class_name)) or not hasattr(eval(class_name), attribute):
            return None

        current_value = getattr(eval(class_name), attribute)
        ## Take care of multiple values.
        if isinstance(current_value, list):
            if not isinstance(values, list):
                new_value = [values]
            elif values.__len__() > 1:
                new_value = values
            elif values.__len__() == 0:
               new_value = []
            else:
                new_value = [values[0]]
        else:
            if not isinstance(values, list):
                if values == '':
                    return None
                new_value = values
            elif values.__len__() == 0:
                new_value = ''
            elif values.__len__() == 1:
                new_value = values[0]
            else:
                print colout.warn("  Too many values found for attribute " + str(class_name) + "." + str(attribute)\
                                  + ". Keeping the default.")
                return None

        return self.copyVariableType(current_value, new_value)


    def copyVariableType(self, model, copy):
        """
        Tries to convert the copy variable' type to the model's.
        :param model:
        :param copy:
        :return:
        """
        try:
            if type(model) == type(copy):
                return copy
            elif isinstance(model, bool):
                if str(copy).lower() in self.TRUE_VALUES:
                    return True
                elif str(copy).lower() in self.FALSE_VALUES:
                    return False
                else:
                    return type(model)(copy)
            else:
                return type(model)(copy)
        except ValueError:
            print "Expected type ", type(model), " for ", copy, " (model value: ", model, ")."
            return None


    def updateConfiguration(self, workingDirectory):
        """
        Fetches the configuration file in the specified working directory then updates variables according to the
        specified classes.
        :param workingDirectory:
        :return:
        """
        print colout.notice("\n[UPDATE CONFIGURATION]")
        if not os.path.isdir(workingDirectory):
            print colout.error(" folder \"" + workingDirectory + "\" does not exist.", with_header=True)
            return None

        configFile = fm.build_dir_path(workingDirectory) + IlluminaConfig.CONFIG_FILE_NAME
        if not os.path.isfile(configFile):
            print colout.warn("\nWarning: ") + "No configuration file found in working directory. Please generate one or the default will"\
                + " be taken. See the command help."
            return 1

        from tools.parameterization.metadata_reader import MetadataReader
        mdr = MetadataReader(configFile)
        for metadata in mdr.getAllMetadataDict().keys():
            self.updateClass(metadata, mdr.getAllKeyValuesFromMd(metadata, key_regex=re.compile("^\w*$")))
        if self.checkMandatoryFields() is None:
            return None
        return 1


    def checkMandatoryFields(self):
        """
        Checks mandatory fields are defined and nto empty.
        :return:
        """
        print colout.notice("\n[CHECKING MANDATORY FIELDS]")
        for field in self.MANDATORY_FIELDS:
            current_value = self.evaluate(field)
            if current_value is None:
                print colout.error(su.stringify("Mandatory Field ", field, " not found."))
                return None
            elif str(current_value).strip(" ").__len__() == 0:
                print colout.error(su.stringify("Mandatory Field ", field, " is empty."))
                return None
            else:
                print field, " = ", current_value
        return 1


    def writeIniConfigFile(self, filePath):
        """
        Creates a new config file in the specified folder.
        :param filePath:
        :return:
        """
        print colout.notice("\n[WRITING CONFIG FILE] ") + str(filePath)
        dirPath = os.path.dirname(filePath)
        if not os.path.isdir(dirPath):
            print colout.error("folder \"" + str(dirPath) + "\" does not exist.")
            return None
        try:
            with open(filePath, 'wb') as configFile:
                content = ""
                firstLine = True
                for class_name in self.getFilteredClassNames(self._class_names):
                    ## Ignore this class attributes. Only the other classes are considered.
                    if firstLine is False:
                        content += "\n\n"
                    else:
                        firstLine = False
                    content += "[" + class_name + "]"
                    attributes = sorted(self.getClassVariables(class_name))
                    for attribute in attributes:
                        value = self.getValueOfClassAttribute(class_name, attribute)
                        if isinstance(value, list):
                            content += "\n" + attribute + " = " + ", ".join(value)
                        else:
                            content += "\n" + attribute + " = " + str(self.getValueOfClassAttribute(class_name, attribute))
                configFile.write(content)
            fm.change_file_permission(filePath, IlluminaConfig.CREATED_FILES_PERMISSION, update_owner=True)
        except IOError as e:
            msg = colout.error("\n[ERROR WRITING FILE] Config path " + str(filePath) + ": \n\t-> " + str(e.strerror))
            return None



def main(argv=None):
    pass

    # print hasattr(eval("IlluminaConfig"), "CREATED_FILES_PERMISSION")
    # print inspect.isclass(eval("ProjectConf"))
    # eval("ProjecR")
    # print inspect.isclass("Por")
    # print inspect.isclass("IlluminaConfig")
    # conf = Configuration()
    # print conf._getNewClassAttributeValue("IlluminaConfig", "FCID_POSITION_IN_RUN_NAME", '')

if __name__ == "__main__":
    sys.exit(main(sys.argv))

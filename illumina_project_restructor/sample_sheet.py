__author__ = 'alexandre-nadin'

import sys
import re
from tools.parameterization.metadata_reader import MetadataReader


class SampleSheet:

    SAMPLE_SHEET_NAME = "SampleSheet"
    SAMPLE_SHEET_EXTENSION = ".csv"
    SAMPLE_SHEET_SEPARATOR = ","
    DATA_HEADER = "Data"


    def __init__(self, file_name, separator=',', oldModel=False):
        self._oldModel = oldModel
        self.sampleSheetPath = file_name
        self.validSampleSheet = True
        self.separator = separator
        self._data_header = ""  # Contains the header as a string
        self._data_header_fields = []  # Contains each field in the data header
        self._data_rows = []  # Contains the data of each sample as strings
        self._data_rows_fields = [] # Contains each field in each data row.
        self._rowErrors = []
        self._headerErrors = []

        print "\n[READING SAMPLE SHEET] ", file_name
        data = []
        mdr = MetadataReader(file_name)
        if mdr.hasMetadata():
            data = mdr.getRawDataFromMd(self.DATA_HEADER)
            print "got metadata!"
        else:
            print "no metadata!"
            with open(file_name, 'rb') as file:
                for line in file:
                    data.append(line)
        self.parse_data(data)


    def parse_data(self, data):
        """
        Parse an array of data. Assumes there is NO metadata.

        :param data: The csv reader containing only the data fields and its raw data.
        :return:
        """
        if data.__len__() < 2:
            self._rowErrors.append("There is no data in sample sheet.")
            self.validSampleSheet = False
        else:
            for row in data:
                ## Get all the fields in a line and strips them.
                fields = map(str.strip, row.split(self.SAMPLE_SHEET_SEPARATOR))

                ## Ignore empty lines
                if fields.__len__() == 0 or (fields.__len__() == 1 and len(str(fields[0]).strip()) == 0):
                    continue

                ## Initiate the data header.
                if len(self._data_header) == 0:
                    # Check there are no empty field
                    empty_fields = 0
                    for field in fields:
                        if str(field).strip(" ").__len__() == 0:
                            self.validSampleSheet = False
                            empty_fields += 1

                    ## Check if empty fields. Continue parsing anyway.
                    if empty_fields > 0:
                        self._headerErrors.append("The header has %d empty field(s)." %(empty_fields))
                        self.validSampleSheet = False
                    self._data_header = self.SAMPLE_SHEET_SEPARATOR.join(fields)
                    self._data_header_fields = fields
                else:
                    ## Check if number of fields correspond
                    data_row = self.SAMPLE_SHEET_SEPARATOR.join(fields)
                    if self._data_header_fields.__len__() == fields.__len__():
                        self._data_rows.append(data_row)
                        self._data_rows_fields.append(fields)
                    else:
                        self.validSampleSheet = False
                        self._rowErrors.append(data_row)


    def is_valid_sample_sheet(self):
        return self.validSampleSheet


    def print_data(self):
        """
        Prints teh header and its data.
        """
        print "\n[HEADER]\n." + self._data_header + "."
        print "\n[ROWS]"
        for row in self._data_rows:
            print "." + row + "."


    def print_summary(self):
        if self._data_header.__len__() == 0:
            print "\n[WARNING] No data found"
        if self.is_valid_sample_sheet():
            print "\n[SAMPLE SHEET VALIDATED] Number of lines found: ", self._data_rows.__len__()
        else:
            print "\n[INVALID SAMPLE SHEET]"
            if self._headerErrors.__len__() > 0:
                print "There are errors with the header: "
                for e in self._headerErrors:
                    print "\t* ", e
            if self._rowErrors.__len__() > 0:
                print "Those lines don't have the same number of field as the header: "
                for e in self._rowErrors:
                    print "\t* ", e, "(", len(str(e).split(self.separator)), "/", self._data_header_fields.__len__(), ")"


    def get_data_header(self):
        return self._data_header


    def get_data_header_fields(self):
        return self._data_header_fields


    def get_data_rows(self):
        return self._data_rows


    def get_data_rows_fields(self):
        return self._data_rows_fields


def runTests(argv=None):
    ## Test pattern matching

    # path = "/Users/alexandre-nadin/dev/illumina-project-restructor/tests/lustre1/SampleSheets/"
    path = "/Users/alexandre-nadin/dev/tests/sample-sheets/"
    fileNames = [
        "test-no-metadata.csv"
        ,"test-with-metadata-end.csv"
        ,"test-with-metadata-between.csv"
        ,"test-with-metadata-no-data.csv"
        ,"test-with-metadata-between-emtpy.csv"
        , "old-with-empty-line-n-spaces.csv"
        , "new-with-empty-line-n-spaces.csv"

    ]

    sheet = SampleSheet(path + fileNames[6])
    sheet.print_data()

    sheet = SampleSheet(path + fileNames[5])
    sheet.print_data()


def main(argv=None):
    runTests(argv)
    # test(path + "151104_SN859_0242_AHTKFYADXX_IEM.csv")


if __name__ == "__main__":
    print "[SampleSheet] main function"
    sys.exit(main(sys.argv))
__author__ = 'alexandre-nadin'

from multiprocessing import Lock
import fcntl

if __name__ == "__main__":
    dirs = [
        "/Users/alexandre-nadin/dev/tests/lustre2/raw_data/Runtest/VanAnken_289_RNASeqLite/"
    ]
    files = [
        "5_L001_R1.fastq.gz"
    ]
    lineCounter = []

    lck = Lock()
    with open(dirs[0]+files[0],mode='rb'):
        lck.acquire()
        raw_input("Enter to proceed")
        lck.release()
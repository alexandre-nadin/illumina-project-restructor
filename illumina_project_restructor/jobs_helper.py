from __future__ import print_function
from tools import computer_utils as cu
import os

from tools.pipeline.pipeline import PipedCommands, PipelineState

__author__ = 'Alexandre Nadin'


class JobFolder:
    MAIN_DIR = ".jobs"
    JOBS_RUNNING = "running"
    JOBS_OVER = "over"
    JOBS_FAILED = "failed"
    JOBS_LIST = "jobs"
    JOBS_OUTPUT = "output"
    EXT_JOB_ID = ".pid"
    EXT_JOB_STDOUT = ".OU"
    EXT_JOB_STDERR = ".ER"

    def __init__(self, wd):
        self._wd = wd
        self.init_job_dir()

    @classmethod
    def get_job_dirs(cls):
        return [cls.JOBS_RUNNING, cls.JOBS_OVER, cls.JOBS_FAILED, cls.JOBS_OUTPUT, cls.JOBS_LIST]

    def get_jobs_path(self, jobs_folder):
        return os.path.join(self._wd, self.MAIN_DIR, jobs_folder)

    def init_job_dir(self):
        """
        Initializes the job directory and subdirectories.
        :return:
        """
        dir_jobs = os.sep.join([self._wd, self.MAIN_DIR])
        sub_dirs = [os.sep.join([dir_jobs, sub_path]) for sub_path in self.get_job_dirs()]

        for a_dir in [dir_jobs] + sub_dirs:
            if not os.path.isdir(a_dir):
                os.mkdir(a_dir)
        return PipelineState.SUCCESS

    def get_jobs(self, jobs_path):
        """
        Returns a list of jobs that are in the specified jobs directory.

        :param jobs_path:
        :return:
        """
        jobs_path = self.get_jobs_path(jobs_path)
        return map(lambda x: os.path.join(jobs_path, x), os.listdir(jobs_path))

    def write_job_file(self, file_name, content=""):
        """
        Writes a job file in the correct job directory.

        :param file_name:
        :param content: content to write in the file.
        :return: The job file's path.
        """
        file_path = os.path.join(self.get_jobs_path(self.JOBS_LIST), file_name)
        write_file(file_path, content)
        return file_path

    def update_job(self, pid, to_job_folder, from_job_folder=None, content=""):
        """
        Moves a job to the specified folder.
        If no origin is set, creates a new file to the specified destination.
        :param pid:
        :param to_job_folder:
        :param from_job_folder:
        :param content:
        :return:
        """
        to_file = os.path.join(self.get_jobs_path(to_job_folder), pid)
        if from_job_folder is None:
            write_file(to_file, content)
        else:
            from_file = os.path.join(self.get_jobs_path(from_job_folder), pid)
            os.rename(from_file, to_file)


class JobScheduler:
    PBS, BASH = 0, 1
    OUTPUT_DIR = "output"

    @classmethod
    def get_job_scheduler(cls):
        if machine_has_pbs():
            return cls.PBS
        else:
            return cls.BASH


class JobManager:
    def __init__(self, wd, job_scheduler=None):
        self._wd = wd
        self._job_scheduler = JobScheduler.get_job_scheduler() if job_scheduler is None else job_scheduler
        self._job_folder = JobFolder(self._wd)

    def create_job(self, file_name, content):
        """
        Creates a job in the right directory.
        :param file_name:
        :param content:
        :return: The job file's path.
        """
        return self._job_folder.write_job_file(file_name, content)

    def submit_jobs(self):
        return self.submit_job_files(self._job_folder.get_jobs(JobFolder.JOBS_LIST))

    def submit_job_files(self, file_list):
        """
        Submits a list of files as jobs to execute..
        :param file_list: a list of job paths
        :return:
        """
        pids = []
        for job in file_list:
            print("submitting job:", job)
            cmd = get_launching_command(job, self._job_scheduler, output_dir=self._job_folder.get_jobs_path(JobFolder.JOBS_OUTPUT))
            pc = PipedCommands([cmd]).execute()
            pid = pc.get_pipeline_result()[0].strip()
            self._job_folder.update_job(pid, to_job_folder=JobFolder.JOBS_RUNNING)
            pids.append(pid)

        if pids.__len__() > 0:
            # print("pids:", ";".join(pids))
            print("pids:", pids)
        return dict(zip(file_list, pids))

    def update_running_jobs(self):
        for pid in map(lambda x: os.path.basename(x), self._job_folder.get_jobs(JobFolder.JOBS_RUNNING)):
            self.update_running_job(pid)

    def update_running_job(self, pid):
        if self.check_process(pid) != 0:
            self._job_folder.update_job(pid, to_job_folder=JobFolder.JOBS_OVER, from_job_folder=JobFolder.JOBS_RUNNING)

    def check_process(self, pid):
        """
        :param pid:
        :return:
        """
        cmd = get_pid_checking_command(pid, self._job_scheduler)
        print("checking pid:", pid)
        pc = PipedCommands([cmd]).execute()
        # print("  results:", pc.get_pipeline_result())
        print("  status:", pc.get_pipeline_status())
        return pc.get_pipeline_status()

    # for pid in jobs_pids_dict.values():
    #         if check_process(pid) != 0:

    def is_jobs_over(self):
        return self._job_folder.get_jobs(JobFolder.JOBS_RUNNING).__len__() == 0

    def get_summary(self):
        return "\n-JOBS {}\n\n-RUNNING {}\n\n-OVER {}\n\n-FAILED {}\n" \
              .format(self._job_folder.get_jobs(JobFolder.JOBS_LIST)
                      , self._job_folder.get_jobs(JobFolder.JOBS_RUNNING)
                      , self._job_folder.get_jobs(JobFolder.JOBS_OVER)
                      , self._job_folder.get_jobs(JobFolder.JOBS_FAILED))


def machine_has_pbs():
    return cu.is_cmd("qsub")


# def check_process(pid, job_scheduler=JobScheduler.PBS):
#     """
#     if pid in folder running
#
#         if pid is running
#
#     :param pid:
#     :param job_scheduler:
#     :return:
#     """
#     cmd = get_pid_checking_command(pid, job_scheduler)
#     print("checking pid:", pid)
#     pc = PipedCommands([cmd]).execute()
#     # print("  results:", pc.get_pipeline_result())
#     print("  status:", pc.get_pipeline_status())
#     return pc.get_pipeline_status()


def get_pid_checking_command(pid, job_scheduler=JobScheduler.PBS):
    """
    Returns the command used to check on a process id depending on the specified job scheduler.
    :param pid:
    :param job_scheduler:
    :return:
    """
    if job_scheduler == JobScheduler.PBS:
        return "qstat {}".format(pid)
    else:
        return ""


def get_launching_command(job_path, job_scheduler=JobScheduler.PBS, output_dir=None):
    job_folder = os.path.dirname(job_path)
    output_dir = os.path.join(job_folder, JobScheduler.OUTPUT_DIR) if output_dir is None else output_dir
    if not os.path.isdir(output_dir):
        os.mkdir(output_dir)

    if job_scheduler == JobScheduler.PBS:
        return "qsub -o {} -e {} {}".format(output_dir, output_dir, job_path)
    else:
        return ""


def touch(file_path):
    """
    Equivalent to Unix touch.
    :param file_path:
    :return:
    """
    with open(file_path, 'a'):
        os.utime(file_path, None)


def write_file(file_path, content):
    with open(file_path, 'w') as f:
        f.write(content)


def get_example_job_cmds(serial):
    return [
            "#!/bin/bash"
            , "echo \"hey I am job Nb. {}\"".format(serial)
            , "echo \"I am here: $(pwd)\""
            , "echo \"python version: $(python --version)\""
            , "echo \"pipestatus: ${PIPESTATUS[@]}\""
            ]


def tests():
    # working_dir = "/Users/alexandre-nadin/dev/tests/pbs"

    pbs_files = []
    working_dir = "/home/anadin/dev/tests/pbs"

    job_manager = JobManager(working_dir)

    for x in range(1, 10):
        pbs_file_name = "job_{}.pbs".format(x)
        pbs_files.append(job_manager.create_job(pbs_file_name, content=os.linesep.join(get_example_job_cmds(serial=x))))

        print("Written pbs file:", pbs_file_name)

    # jobs_pids_dict = job_manager.submit_job_files(pbs_files)
    jobs_pids_dict = job_manager.submit_jobs()

    print("\nJoining {} processes...".format(jobs_pids_dict.__len__()))
    import time
    counter = 0
    joined_counter = 0

    while not job_manager.is_jobs_over():
        if counter > 0:
            time.sleep(1)
        counter += 1
        print("\n[CHECKING PROCESSES] try nb. {} ...".format(counter))
        job_manager.update_running_jobs()

    # while joined_counter != jobs_pids_dict.__len__():
    #     joined_counter = 0
    #     if counter > 0:
    #         time.sleep(1)
    #
    #     counter += 1
    #     print("\n[CHECKING PROCESSES] try nb. {} ...".format(counter))
    #     for pid in jobs_pids_dict.values():
    #         if check_process(pid) != 0:
    #             joined_counter += 1
    #     print("joined {} / {} jobs".format(joined_counter, jobs_pids_dict.__len__()))

    print("\n[SUMMARY]:\n", job_manager.get_summary())

if __name__ == "__main__":
    tests()


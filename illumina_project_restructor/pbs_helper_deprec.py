__author__ = 'Alexandre Nadin'

from multiprocessing import Queue, Process
from tools import computer_utils as cu
import subprocess
import time
import os



def machine_has_pbs():
    return cu.is_cmd("qsub")

def test_jobs(max_jobs=100):
    queue_results = Queue()  # Contains tuples of (n-th position, SampleFile() )
    job_list = []
    max_range = 200
    min_range = 1
    counter = 0
    counter_try = 0
    submit_delay = 0.1
    jobs_per_try = 50

    original_contents = []
    for i in range(min_range, max_range+1):
        original_contents.append("Content Nb. " + str(i))

    print_content(original_contents)

    print "\nTest processing", max_jobs, "jobs in parallel."
    for position, content in enumerate(original_contents):
        counter += 1
        counter_try += 1
        if counter > max_jobs:
            break
        p = Process(target=test_process, args=(position, content, queue_results))
        job_list.append(p)
        if counter_try >= jobs_per_try:
            test_process_some_jobs(job_list)

            ## Update results
            update_list(original_contents, queue_results)

            ## Reset counters
            counter_try = 0
            job_list = []

        time.sleep(submit_delay)

    ## Process last jobs
    if job_list.__len__() > 0:
        test_process_some_jobs(job_list)
        update_list(original_contents, queue_results)

    print_content(original_contents)

    print "\ntest over. Exiting.\n"


def update_list(original_list, queue_results):

    result_tuples = []
    while not queue_results.empty():
        result_tuples.append(queue_results.get())
    print "  results appended"
    for position, content in result_tuples:
        original_list[position] = content

def test_process_some_jobs(jobs_list):
    print "  processing", jobs_list.__len__(), "jobs at a time"

    for p in jobs_list:
        p.start()

    for p in jobs_list:
        p.join()

    return 1


def print_content(alist):
    msg = "\nPrint content:"
    for content in alist:
        msg += "\n + " + str(content)
    print msg

def test_process(position, content, queue_results):
    listing = []
    for x in range(0, 100000):
        listing.append(os.listdir("/home"))
    queue_results.put((position, content + " -> processed! Len=" + str(listing.__len__())))
    # import random
    # queue_results.put((position + random.randint(1, 5), content + " -> processed!"))


if __name__ == "__main__":
    import sys
    max_jobs = None
    try:
        max_jobs = int(sys.argv[1])
    except:
        pass

    if max_jobs is None:
        test_jobs()
    else:
        test_jobs(max_jobs)

